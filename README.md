#  Centre Flowed Text and Inkscape extension

▶ Centres flowed text in its frame,
  for example after resizing frame

▶ Works with individually selected text flow elements
  or will find flow text in multiple selections

▶ Appears in 'Extensions>Text>Centre Flowed Text'

▶ A shortcut can be used to
  inklinea.center_flowed_text.noprefs
  
▶ Inkscape 1.2.1+

