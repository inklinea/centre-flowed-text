<?xml version="1.0" encoding="UTF-8"?>

<!--Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]-->

<!--        This program is free software; you can redistribute it and/or modify-->
<!--        it under the terms of the GNU General Public License as published by-->
<!--        the Free Software Foundation; either version 2 of the License, or-->
<!--        (at your option) any later version.-->

<!--        This program is distributed in the hope that it will be useful,-->
<!--        but WITHOUT ANY WARRANTY; without even the implied warranty of-->
<!--        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the-->
<!--        GNU General Public License for more details.-->

<!--        You should have received a copy of the GNU General Public License-->
<!--        along with this program; if not, write to the Free Software-->
<!--        Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.-->


<!--        #############################################################################-->
<!--        #  Centre Flowed Text and Inkscape extension-->
<!--        #  Centres flowed text in its frame, for example after resizing frame-->
<!--        #  Appears under Extensions>Text>Centre Flowed Text-->
<!--        #  A keyboard shortcut can be made to inklinea.center_flowed_text.noprefs-->
<!--        #  Requires Inkscape 1.2+-->
<!--        #############################################################################-->

<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
    <name>Centre Flowed Text</name>
    <id>inklinea.center_flowed_text</id>
    
    <!--  Parameters Here -->

    <param name="center_flowed_text_notebook" type="notebook">

        <page name="settings_page" gui-text="Settings">

    <hbox>
    <param name="centering_radio" type="optiongroup" appearance="radio" gui-text="">
        <option value="horizontal">Horizontal</option>
        <option value="vertical">Vertical</option>
        <option value="both">Both</option>
    </param>
    </hbox>

    <label appearance="header">Method</label>

    <hbox>
    <param name="bounding_box_radio" type="optiongroup" appearance="radio" gui-text="">
        <option value="textparser">TextParser Bounding Box</option>
        <option value="query_all">Inkscape --query-all Bounding Box</option>
    </param>
    </hbox>
<!--        end of settings page-->
        </page>

        <page name="about_page" gui-text="About">

            <label>
                Centre Flowed Text and Inkscape extension
            </label>
            <label>
                Inkscape 1.2.1+
            </label>
            <label appearance="url">

                https://inkscape.org/~inklinea/resources/=extension/

            </label>

            <label appearance="url">

                https://gitlab.com/inklinea

            </label>

            <label xml:space="preserve">
▶ Centres flowed text in its frame,
  for example after resizing frame
▶ Works with individually selected text flow elements
  or will find flow text in multiple selections
▶ Appears in 'Extensions>Text>Centre Flowed Text'
▶ A shortcut can be used to
  inklinea.center_flowed_text.noprefs

		</label>
        </page>


<!--        end of notebook-->

    </param>
	
    <effect>
        <object-type>all</object-type>
        <effects-menu>
            <submenu name="Text"/>
        </effects-menu>
    </effect>
    <script>
        <command location="inx" interpreter="python">center_flowed_text.py</command>
    </script>
</inkscape-extension>
