import inkex

# Only import standard libraries here
# Otherwise if a system with
import random
import time


class Inklin:

    #Unit conversions
    conversions = {
        'in': 96.0,
        'pt': 1.3333333333333333,
        'px': 1.0,
        'mm': 3.779527559055118,
        'cm': 37.79527559055118,
        'm': 3779.527559055118,
        'km': 3779527.559055118,
        'Q': 0.94488188976378,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0,
        '': 1.0,  # Default px
    }

    # Workaround for Inkscape 1.1+ shape/image units bug.
    def shape_unit_fix(self):
        """
        Inkscape 1.1 returns paths in user units but shapes and images in pixels. \n
        This is a user selectable conversions factor ( usually a checkbox ) \n
        which is stored at self.conversion_factor
        """
        found_units = self.svg.unit
        if self.options.shape_unit_fix == 'true':
            self.conversion_factor = Inklin.conversions[found_units]
        else:
            self.conversion_factor = 1

    # Could not find simplestyle, found this instead in extensions repo
    def formatStyle(a):
        """Format an inline style attribute from a dictionary"""
        return ";".join([att + ":" + str(val) for att, val in a.items()])

    # Platform Check
    ################
    def os_check(self):
        """
        Check which OS we are using
        :return: OS Name ( windows, linux, macos )
        """
        from sys import platform

        if 'linux' in platform.lower():
            return 'linux'
        elif 'darwin' in platform.lower():
            return 'macos'
        elif 'win' in platform.lower():
            return 'windows'

    # Function to list object attributes
    ####################################

    def get_attributes(self):
        """ Returns a string containing all object attributes
             - One attribute per line
        """
        attribute_string = 'test'
        for att in dir(self):
            try:
                attribute = (att, getattr(self, att))
                attribute_string = attribute_string + str(attribute) + '\n'
            except:
                None
        return attribute_string

    # Extra path functions
    #######################

    # Return a rectangle path

    # Create Groups
    def create_new_group(self, parent, prefix, mode, suffix_type='epoch'):
        """
        Create a new group or Inkscape layer in document and return group

        :param parent: Parent group will be appended to
        :param prefix: The prefix for the group name
        :param mode: Inkscape 'layer' or 'group'
        :param suffix_type: The suffix for the group name
        :return: Group Object
        """

        from inkex import Group

        if suffix_type == 'random':
            id_suffix = str(random.randrange(1000000, 9999999))
        if suffix_type == 'epoch':
            id_suffix = str(time.time())

        group_id = str(prefix) + '_' + id_suffix
        new_group = parent.add(Group.new(group_id))
        new_group.set('inkscape:groupmode', str(mode))
        new_group.attrib['id'] = group_id

        return new_group

    # Colour functions
    ##################

    def random_rgb_string(self):
        """
        Return a random rgb colour string \n
        rgb(0-255, 0-255, 0-255)
        """
        random_red = random.randrange(0, 255)
        random_green = random.randrange(0, 255)
        random_blue = random.randrange(0, 255)

        return f'rgb({random_red}, {random_green}, {random_blue})'

    # Could not find simplestyle, found this instead in extensions repo
    def format_style(my_style):
        """Format an inline style attribute from a dictionary"""
        return ";".join([att + ":" + str(val) for att, val in my_style.items()])

    # Image functions
    #################

    def embedded_image_to_PIL(self, embedded_image):
        """
        If <image> is embedded, do base64string to PIL image
        """

        from PIL import Image
        from io import BytesIO
        import base64

        my_href = embedded_image.get('xlink:href')
        base64_string = my_href.split('base64,')[1]
        img_stream = BytesIO()
        img_stream.write(base64.b64decode(base64_string))
        im = Image.open(img_stream)
        return im

    def linked_image_to_pil(self, image_path):
        """
        If <image> is linked, open and return PIL image.
        """
        from PIL import Image
        im = Image.open(image_path)
        # inkex.errormsg((im.format, im.size, im.mode))
        im.close()
        return im

    def image_to_pil(self, image_element):
        """
        Return a PIL image from an <image> ( linked or embedded )
        """
        if image_element.TAG.lower() == 'image':
            # base64 in string indicated embedded image
            if 'base64' in image_element.tostring().decode('utf-8').lower():
                im = Inklin.embedded_image_to_PIL(self, image_element)
                return im

            # Otherwise it will be a linked image
            else:
                try:
                    image_path = image_element.get('xlink:href')
                    image_real_path = image_path.split('file://')[1]
                    im = Inklin.linked_image_to_pil(self, image_real_path)
                    return im
                except:
                    return None

    def pil_to_base64_image_string(self, im):
        """
        Return a png base64 string from a PIL image
        :param im: PIL image
        :return: Base64 encoded png image
        """
        from PIL import Image
        from io import BytesIO
        import base64

        img_stream = BytesIO()
        im.save(img_stream, format='PNG')
        byte_img = img_stream.getvalue()
        base64_img_str = base64.b64encode(byte_img).decode('utf-8')

        return base64_img_str

    def pil_to_base64_image_object(self, parent, base_id, im):
        """
        Create an <image> object with base64 string (embedded) png image on canvas \n
        :param base_id: The id of the created object
        :param im: The PIL source image
        """
        from lxml import etree
        # Inscape 1.1
        conversion_factor = self.conversion_factor

        my_image = etree.SubElement(self.svg, inkex.addNS('image', 'svg'))

        conversion_factor = self.conversion_factor

        base64_img_str = Inklin.pil_to_base64_image_string(self, im)

        # Lets place the preview image off the canvas
        my_image.attrib['x'] = str(0)
        my_image.attrib['y'] = str((-(im.size[1]) / conversion_factor))

        my_image.attrib['width'] = str(im.size[0] / conversion_factor)
        my_image.attrib['height'] = str(im.size[1] / conversion_factor)

        my_image.attrib['id'] = f'{base_id}'

        my_image.set('xlink:href', str(f'data:image/png;base64,{base64_img_str}'))




    # Command Line
    ##############

    # Return a svg file copy in a temp folder
    # This is due to permission problems with tempfile in Windows

    def make_temp_folder(self):
        """
        Creates a temp folder to which files can be written \n
        To remove folder at end of script use: \n
        # Cleanup temp folder \n
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)

        :return: A temp folder path string
        """
        import tempfile
        temp_folder = tempfile.mkdtemp()
        self.inklin_temp_folder = temp_folder
        return temp_folder

    def make_temp_file_copy(self, my_file, extension='.svg'):
        import shutil
        import os
        import time
        temp_file_name = str(time.time()).replace('.', '') + extension
        if hasattr(self, 'inklin_temp_folder'):
            temp_folder = self.inklin_temp_folder
        else:
            temp_folder = Inklin.make_temp_folder(self)
        temp_file = shutil.copy(my_file, os.path.join(temp_folder, temp_file_name))
        temp_file_object = open(temp_file)
        temp_file_object.temp_folder = temp_folder
        return temp_file_object

    def inkscape_command_call(self, input_file, options_list, action_list):
        """
        A function to execute an Inkscape command call on a temp_file copy of the
        input_file and return the resulting file object. \n
       :param input_file: input file path ( usually self.options.input_file )
       :param options_list: inkscape command line options
       :param action_list: inkscape command line actions ( and verbs in Inskcape 1.1 )
       :return: Returns the resulting svg file object ( which must be .closed() later )
       """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        command.inkscape(temp_svg.name, options_list, f'--actions={action_list}')
        return temp_svg

    def inkscape_command_call_stdout(self, input_file, options_list, action_list):
        """
        A function to execute an Inkscape command call on a temp_file copy of the
        input_file and return stdout only. Useful for select-list etc.\n
        :param input_file: input file path ( usually self.options.input_file )
        :param options_list: inkscape command line options
        :param action_list: inkscape command line actions ( and verbs in Inskcape 1.1 )
        :return: Returns the stdout from the command line call
        """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        stdout = command.inkscape(temp_svg.name, options_list, f'--actions={action_list}')
        temp_svg.close()
        return stdout

    def inkscape_command_call_bboxes_to_dict(self, input_file):
        """
        A function to return a dictionary of all element bounding boxes
        -- this function is visual ( includes stroke etc ) rather than just
        geometric ( just path bbox ) \n
        :param input_file: input file path ( usually self.options.input_file )
        :return: Returns the results of --query-all in a dictionary
        """
        from inkex import command
        # First make a copy of the input_file
        temp_svg = Inklin.make_temp_file_copy(self, input_file)
        # Then run the command line
        my_query = command.inkscape(temp_svg.name, '--query-all')

        # Account for versions of inkey.py which return query as bytes
        if type(my_query) != str:
            my_query = my_query.decode("utf-8")
        # --query-all produces multiline output of the following format
        # path853,172.491,468.905,192.11,166.525 - as string
        # ElementId, Top, Left, Width, Height

        # Make a list splitting by each new line
        my_query_items = my_query.split('\n')
        my_element_bbox_dict = {}

        for my_query_item in my_query_items:
            # Create a comma separated list item for each line
            my_element = my_query_item.split(',')
            # Make a dictionary for all elements, rejected malformed elements.
            if len(my_element) > 4:
                my_element_bbox_dict[my_element[0]] = {}
                # Create Dictionary entry in anticlockwise format
                # x1 = TopLeft, x2 = BottomLeft, x3 = BottomRight, x4 = TopRight, mid_x and mid_y

                # First convert all values to float, skipping element id ( first entry )
                my_element_bbox = [float(x) for x in my_element[1:]]

                width = my_element_bbox[2]
                height = my_element_bbox[3]

                x1 = my_element_bbox[0]
                y1 = my_element_bbox[1]
                x2 = x1
                y2 = y1 + height
                x3 = x1 + width
                y3 = y2
                x4 = x1 + width
                y4 = y1
                mid_x = x1 + width / 2
                mid_y = y1 + height / 2

                my_element_bbox_dict[my_element[0]].update(x1=x1, y1=y1, x2=x2, y2=y2, x3=x3, y3=y3, x4=x4, y4=y4,
                                                           mid_x=mid_x, mid_y=mid_y, width=width, height=height)
        # Return dictionary
        temp_svg.close()
        return my_element_bbox_dict

