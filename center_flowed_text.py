#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
#  Centre Flowed Text and Inkscape extension
#  Centres flowed text in its frame, for example after resizing frame
#  Appears under Extensions>Text>Centre Flowed Text
#  Requires Inkscape 1.2+ -->
# #############################################################################

import inkex

from inklinea import Inklin

from inkex import Transform, Rectangle

import uuid, os, shutil, sys

# Import David Burghoff TextParser
# https://github.com/burghoff/Scientific-Inkscape/tree/main/scientific_inkscape
sys.path.insert(-1, './textparser')
from TextParser import *


def get_textparser_bbox(self, text_object, bboxes):
    text_object_id = text_object.get_id()

    parsed_text = get_parsed_text(text_object)

    textparser_bbox = parsed_text.get_full_inkbbox()

    width = textparser_bbox.w
    height = textparser_bbox.h
    x1 = textparser_bbox.x1
    x2 = x1
    y1 = textparser_bbox.y1
    y2 = y1 + height
    x3 = x1 + width
    y3 = y2
    x4 = x1 + width
    y4 = y1
    mid_x = x1 + width / 2
    mid_y = y1 + height / 2

    bboxes[text_object_id] = {}

    bboxes[text_object_id].update(x1=x1, y1=y1, x2=x2, y2=y2, x3=x3, y3=y3, x4=x4, y4=y4,
                                  mid_x=mid_x, mid_y=mid_y, width=width, height=height)

    return bboxes

def get_standard_object_bbox(self, my_object):

    my_object_bbox = my_object.bounding_box()

    width = my_object_bbox.width
    height = my_object_bbox.height
    x1 = my_object_bbox.left
    x2 = x1
    y1 = my_object_bbox.top
    y2 = y1 + height
    x3 = x1 + width
    y3 = y2
    x4 = x1 + width
    y4 = y1
    mid_x = x1 + width / 2
    mid_y = y1 + height / 2

    object_bbox = {}

    object_bbox.update(x1=x1, y1=y1, x2=x2, y2=y2, x3=x3, y3=y3, x4=x4, y4=y4,
                                mid_x=mid_x, mid_y=mid_y, width=width, height=height)

    return object_bbox


def get_temp_svg_filepath(self):
    new_svg = self.svg.tostring().decode('utf-8')

    temp_folder = self.temp_folder = Inklin.make_temp_folder(self)

    temp_file_name = str(uuid.uuid4()) + '.svg'

    temp_file_path = os.path.join(temp_folder, temp_file_name)

    with open(temp_file_path, mode='w', encoding='utf-8') as file:
        file.write(new_svg)

    return temp_file_path


class CenterFlowedText(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--center_flowed_text_notebook", type=str, dest="center_flowed_text_notebook", default=0)
        
        pars.add_argument("--bounding_box_radio", type=str, dest="bounding_box_radio", default='epoch')

        pars.add_argument("--centering_radio", type=str, dest="centering_radio", default='epoch')


    def effect(self):

        bbox_method = self.options.bounding_box_radio

        units = self.svg.unit
        if bbox_method == 'query_all':
            self.conversion_factor = Inklin.conversions[units]
        else:
            self.conversion_factor = 1

        selection_list = self.svg.selected

        if len(selection_list) < 1:
            return

        flow_text_object_list = []

        for selected_object in selection_list:

            if selected_object.TAG == 'text' and 'shape-inside' in selected_object.style:

                flow_text_object_list.append(selected_object)

                null_transform = Transform().add_translate(0, 0)

                selected_object.y_original_translate = selected_object.transform.f
                selected_object.x_original_translate = selected_object.transform.e

                selected_object.transform = null_transform

                flow_parent_url = selected_object.style['shape-inside']
                flow_parent_id = flow_parent_url.split('url(#')[1].replace(')', '')

                selected_object.flow_parent_id = flow_parent_id

        # Now all flow text elements have had their transforms reset to top left in their frame
        # but there will still be an x, y offset between the frame bbox and the flow text bbox


        if bbox_method == 'query_all':

            temp_filepath = get_temp_svg_filepath(self)
            bboxes = Inklin.inkscape_command_call_bboxes_to_dict(self, temp_filepath)

        else:
            bboxes = {}
            for flowed_text_object in flow_text_object_list:
                bboxes = get_textparser_bbox(self, flowed_text_object, bboxes)


        for flow_text_object in flow_text_object_list:

            flow_parent = self.svg.getElementById(flow_text_object.flow_parent_id)


            if bbox_method != 'query_all':
                frame_bbox = get_standard_object_bbox(self, flow_parent)
            else:
                frame_bbox = bboxes[flow_text_object.flow_parent_id]

            flowed_text_id = flow_text_object.get_id()
            flowed_text_bbox = bboxes[flowed_text_id]

            text_x_offset = flowed_text_bbox['x1'] - frame_bbox['x1']
            text_y_offset = flowed_text_bbox['y1'] - frame_bbox['y1']

            x_shift = frame_bbox['mid_x'] - (frame_bbox['x1']) - (flowed_text_bbox['width'] / 2) - text_x_offset
            y_shift = frame_bbox['mid_y'] - (frame_bbox['y1']) - (flowed_text_bbox['height'] / 2) - text_y_offset

            if self.options.centering_radio == 'both':
                text_x_shift = x_shift / self.conversion_factor
                text_y_shift = y_shift / self.conversion_factor
            elif self.options.centering_radio == 'horizontal':
                text_x_shift = x_shift / self.conversion_factor
                text_y_shift = flow_text_object.y_original_translate
            elif self.options.centering_radio == 'vertical':
                text_x_shift = flow_text_object.x_original_translate
                text_y_shift = y_shift / self.conversion_factor


            text_translate = Transform().add_translate(text_x_shift, text_y_shift)

            flow_text_object.transform = flow_text_object.transform @ text_translate


        # Cleanup temp folder
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)


if __name__ == '__main__':
    CenterFlowedText().run()
